package profe;

public class ZipCodeNotFoundException extends Exception {

	public ZipCodeNotFoundException() {
		super("ZipCode Not Found Exception");
	}

	public ZipCodeNotFoundException(String msg) {
		super(msg);
	}
}

