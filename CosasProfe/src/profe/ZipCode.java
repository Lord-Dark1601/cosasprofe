package profe;

import java.util.Scanner;
import java.io.*;

public class ZipCode {

	public static final String FILE_NAME = "codPos.csv";

	// private static String[] objective = null;

	public static void main(String[] args) {

		try {
			System.out.println(getText(12520));
		} catch (IOException ex) {
			System.out.println("ERROR");
			ex.printStackTrace();
		}
	}

	public static String getText(int zipCode) throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(FILE_NAME));
			String line;

			while ((line = in.readLine()) != null) {
				Scanner scan = new Scanner(line);
				scan.useDelimiter(";");
				String codProv = scan.next();
				String cityOfProvince = scan.next();
				String bigCity = scan.next();
				String zipCodeStr = scan.next();
				String streetOrVillage = scan.next();
				scan.close();

				int zipCodeInt = Integer.parseInt(zipCodeStr);
				if (zipCodeInt > zipCode) {
					break;
				}

				if (zipCode == zipCodeInt) {
					String result = "codProvince: " + codProv + "\n";
					if (bigCity.equalsIgnoreCase("true")) {
						result += "City: " + cityOfProvince + "\n";
						result += "First street: " + streetOrVillage + "\n";
					} else {
						result += "Province: " + cityOfProvince + "\n";
						result += "City: " + streetOrVillage + "\n";
					}
					result += "Zip code: " + zipCodeStr + "\n";
					return result;
				}
			}
			return "Zip code not found";
		} finally {
			if (in != null) {
				in.close();
			}

		}
	}

}